import clients.GETWeatherClient;
import io.restassured.response.Response;
import org.testng.annotations.Test;

public class GETWeather {
    @Test
    public void shouldBeAbleToGetWeather(){
        GETWeatherClient getWeatherClient=new GETWeatherClient();
        Response response=getWeatherClient.getWeather("https://community-open-weather-map.p.rapidapi.com/weather");
        System.out.println(response.body().asString());
    }
}
