import clients.GETBlackProductsClient;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class GETBlackProducts {
    @Test
    public void shouldBeAbleToGteBlackProducts() throws IOException {
        GETBlackProductsClient getBlackProductsClient =new GETBlackProductsClient();
        List<String> blackProducts =getBlackProductsClient.getBlackProducts(System.getProperty("env"));
        System.out.println("Black Coloured products are:"+blackProducts);

    }
}
