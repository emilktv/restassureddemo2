import clients.GETNaturalProductsClient;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class GETNaturalProducts {
    @Test
    public void shouldBeAbleToGetNaturalProducts() throws IOException {
        GETNaturalProductsClient getNaturalProductsClient=new GETNaturalProductsClient();
        List<String> naturalProducts = getNaturalProductsClient.getNaturalProducts(System.getProperty("env"));
        System.out.println("Names of Natural Products"+naturalProducts);
    }
}
