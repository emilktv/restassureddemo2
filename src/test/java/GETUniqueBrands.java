import clients.GETUniqueBrandsClient;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

public class GETUniqueBrands {
    @Test
    public void shouldBeAbleToGetUniqueBrands() throws IOException {
        GETUniqueBrandsClient getUniqueBrandsClient=new GETUniqueBrandsClient();
        HashSet<String> uniqueBrands=getUniqueBrandsClient.getUniqueBrands(System.getProperty("env"));
        System.out.println("Unique Brands:"+uniqueBrands);
        System.out.println("Number of Unique Brands"+uniqueBrands.size());
    }
}
