package clients;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GETWeatherClient {
    public Response getWeather(String apiEndpoint){
        Response response=given()
                .headers("X-RapidAPI-Host","community-open-weather-map.p.rapidapi.com","X-RapidAPI-Key","63f6fa2e89msh245efad066c6b69p1628fbjsn6e8fabee1471")
                .queryParam("q","London,uk")
                .when()
                .get(apiEndpoint);
        return response;
    }
}
