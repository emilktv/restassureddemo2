package clients;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class GETBlackProductsClient {
    public List<String> getBlackProducts(String env) throws IOException {
        Properties properties=new PropertyReader().readProperty(env);
        Response products=given()
                .contentType(ContentType.JSON)
                .when()
                .get(properties.getProperty("basepath")+properties.getProperty("endpoint"));
        JsonPath js = new JsonPath(products.asString());
        List<String> productsList =js.getList("findAll{it.product_colors.findAll {it.colour_name=='Black'}}.name");
        return productsList;
    }
}
