package clients;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class GETUniqueBrandsClient {
    public HashSet<String> getUniqueBrands(String env) throws IOException {
        Properties properties=new PropertyReader().readProperty(env);
        Response products=given()
                .contentType(ContentType.JSON)
                .when()
                .get(properties.getProperty("basepath")+properties.getProperty("endpoint"));
        JsonPath js = new JsonPath(products.asString());
        List<String> allBrands=js.getList("brand");
        System.out.println("All products number:"+allBrands.size());
        HashSet<String> uniqueBrands=new HashSet<String>(allBrands);
        return uniqueBrands;
    }
}
